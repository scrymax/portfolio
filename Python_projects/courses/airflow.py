
from datetime import datetime, timedelta
import pandas as pd
from io import StringIO
import requests
import pandahouse as ph

from airflow.decorators import dag, task
from airflow.operators.python import get_current_context

connection = {
    'host': '_',
    'password': '_',
    'user': '_',
    'database': '_'
}

# Дефолтные параметры, которые прокидываются в таски
default_args = {
    'owner': 'm.osipov',
    'depends_on_past': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'start_date': datetime(2022, 6, 20),
}

# Интервал запуска DAG
schedule_interval = '0 11 * * *'

@dag(default_args=default_args, schedule_interval=schedule_interval, catchup=False)
def dag_osipov():

    @task
    def extract_feed():
        query_feed = """SELECT 
                       toDate(time) as event_date,
                       user_id as user,
                       gender,
                       age,
                       os,
                       countIf(user_id, action='like') as likes,
                       countIf(user_id, action='view') as views

                    FROM 
                        simulator_20220520.feed_actions 
                    where 
                        toDate(time) = today() - 1
                    group by
                        event_date,
                        user,
                        gender,
                        age,
                        os"""

        df_feed = ph.read_clickhouse(query_feed, connection=connection)
        return df_feed

    @task
    def extract_message():
        query_message = """select *
                            from
                            (select
                                toDate(time) as event_date,
                                user_id as user,
                                gender,
                                age,
                                os,
                                count() as message_sent,
                                count(distinct reciever_id) as users_sent
                            from simulator_20220520.message_actions
                            where date(time) = today() - 1
                            group by event_date, user, gender, age, os) as q1

                            left join

                            (select 
                                reciever_id as user,
                                count() as messages_recieved,
                                count(distinct user_id) as users_recieved

                            from simulator_20220520.message_actions
                            where date(time) = today() - 1
                            group by user) as q2
                            using user"""

        df_message = ph.read_clickhouse(query_message, connection=connection)
        return df_message



    @task
    def merge_df(df_1, df_2):
        df = df_1.merge(df_2, on = ['event_date', 'user', 'gender', 'age', 'os'], how = 'outer')
        return df

    @task
    def metric_gender(df_merge):
        df_gender = df_merge[{'event_date', 'gender', 'likes', 'views', 'message_sent', 'users_sent', 'messages_recieved', 'users_recieved'}]\
                    .rename(columns = {'gender':'metric_value'})\
                    .groupby(['event_date','metric_value'], as_index = False)\
                    .sum()
        df_gender['metric'] = 'gender'
        return df_gender


    @task
    def metric_age(df_merge):
        df_age = df_merge[{'event_date', 'age', 'likes', 'views', 'message_sent', 'users_sent', 'messages_recieved', 'users_recieved'}]\
                    .rename(columns = {'age':'metric_value'})\
                    .groupby(['event_date','metric_value'], as_index = False)\
                    .sum()
        df_age['metric'] = 'age'
        return df_age


    @task
    def metric_os(df_merge):
        df_os = df_merge[{'event_date', 'os', 'likes', 'views', 'message_sent', 'users_sent', 'messages_recieved', 'users_recieved'}]\
                    .rename(columns = {'os':'metric_value'})\
                    .groupby(['event_date','metric_value'], as_index = False)\
                    .sum()
        df_os['metric'] = 'os'    
        return df_os



    @task
    def load_table(df1, df2, df3):
        df_final = pd.concat([df1, df2, df3])
        df_final = df_final.astype({'users_recieved':int,\
                                    'views':int,\
                                    'messages_recieved':int,\
                                    'users_sent':int,\
                                    'likes':int,\
                                    'message_sent':int})
        
        connect = {
        'host': '_',
        'password': '_',
        'user': '_',
        'database': '_'}

        query_cr = """CREATE TABLE IF NOT EXISTS test.osipov
                        (event_date Date,
                        metric String,
                        metric_value String,
                        views Int32,
                        likes Int32,
                        message_sent Int32,
                        users_sent Int32,
                        messages_recieved Int32,
                        users_recieved Int32)
                        ENGINE = MergeTree
                        ORDER BY(event_date);

        """
        ph.execute(connection=connect, query = query_cr)

        ph.to_clickhouse(df_final, 'osipov', index=False, connection = connect)


    df_feed = extract_feed()
    df_message = extract_message()
    df_merge = merge_df(df_feed, df_message)
    df_gender = metric_gender(df_merge)
    df_age = metric_age(df_merge)
    df_os = metric_os(df_merge)
    load_table(df_gender, df_age, df_os)

dag_osipov = dag_osipov()

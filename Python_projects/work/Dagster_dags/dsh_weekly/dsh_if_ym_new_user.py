import pandas as pd
import os
from tapi_yandex_metrika import YandexMetrikaStats
from dagster import asset, OpExecutionContext, AssetOut, Output

from ---.jobs.dsh_if_plus_weekly.job_definition import weekly_partitions_def



@asset(
    partitions_def=weekly_partitions_def,
    output_required=False,
    description='Посетители из Я.метрики',
)
def dsh_if_ym_new_user(context: OpExecutionContext) :
    """
    Подключение к яндекс-метрике и получение данных о новых посетителях за неделю
    """

    context.log.info("Try to transform dates...")
    start, end = context.partition_time_window

    context.log.info("Connecting to yandex metrika api...")
    access_token = os.getenv("---")
    metric_ids = "---"
    # По умолчанию возвращаются только 10000 строк отчета,
    # если не указать другое кол-во в параметре limit.

    api = YandexMetrikaStats(
        access_token=access_token
    )

    # Параметры запроса для библиотеки tapi_yandex_metrika
    params = dict(
        ids=metric_ids,
        metrics="ym:s:newUsers",
        dimensions="",
        filters="ym:s:isRobot == 'No'",
        date1=start.date(),
        date2=end.date(),
        sort="",
        accuracy="full",
        limit=500
    )

    # Получаем данные из Yandex.Metrika API
    result = api.stats().get(params=params)
    result = result().data
    result = result['data']

    dict_data = {}
    for i in range(len(result)):
        dict_data[i] = {'users': result[i]["metrics"][0]}

    dict_keys = dict_data[0].keys()
    df = pd.DataFrame.from_dict(dict_data, orient='index',columns=dict_keys)
    df = df.astype({'users': 'int'})

    cnt = int(df.iloc[0][0])

    context.log.info(f"Новых пользователей: {cnt}!")

    if len(df) > 0:
        yield Output(
            df,
            metadata={"Profiles count": cnt}
        )

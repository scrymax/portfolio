import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output

from ---.jobs.dsh_if_plus_weekly.job_definition import weekly_partitions_def
from ---.resources.databases.clickhouse import ClickhouseConnectionProvider


@asset(
    required_resource_keys={"analytical_database"},
    partitions_def=weekly_partitions_def,
    output_required=False,
    description='Первые оплаты тарифов прошлой недели'
)
def tariff_orders_week(context: OpExecutionContext) -> pd.DataFrame:
    """
    Получение информации о впервых оплативших пользователях
    """

    start, end = context.partition_time_window

    context.log.info("Подключаемся к CLickHouse...")
    context.log.info(f"start - {start}, end - {end}")
    provider: ClickhouseConnectionProvider = context.resources.analytical_database

    context.log.info(f"Выгрузка данных...")

    query = f"""
            with 
                first_paym as 
                    (select customer_id c , min(updated_at::Timestamp) as u
                    from orders o
                        join product p on
                            o.product_id = p.id
                    where p.product_type = 'tariff'
                    group by customer_id )
            select o2.id id , o2.price price, o2.updated_at ::Timestamp updated_at, o2.product_id product_id,
                o2.customer_id customer_id, p2.name name
            from orders o2
                join product p2 on
                    o2.product_id = p2.id
                join first_paym fp on
                    o2.customer_id = fp.c and o2.updated_at = fp.u       
            where o2.updated_at::Timestamp between '{start.date()}' and '{end.date()}'
                and  p2.product_type = 'tariff'
                """

    context.log.debug(f"Выполнение запроса:\n{query}")

    df = provider.execute_query_to_dataframe(query)

    context.log.info(f"Запрос выполнен! {len(df)} строк выгружено!")

    if len(df) > 0:
        yield Output(
            df,
            metadata={"Количество первых оплат": len(df)}
        )
    else:
        yield Output(
            pd.DataFrame(),
            metadata={"Количество первых оплат": 0}
        )

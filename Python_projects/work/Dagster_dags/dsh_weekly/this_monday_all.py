import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output

from ---.jobs.dsh_if_plus_weekly.job_definition import weekly_partitions_def
from ---.resources.databases.clickhouse import ClickhouseConnectionProvider


@asset(
    required_resource_keys={"analytical_database"},
    partitions_def=weekly_partitions_def,
    output_required=False,
    description='Все пользователи на этот понедельник'
)
def this_monday_all(context: OpExecutionContext) -> pd.DataFrame:
    """
    Получение ифнормации обо всех пользователях на текущий понедельник
    """

    start, end = context.partition_time_window

    context.log.info("Подключаемся к CLickHouse...")
    provider: ClickhouseConnectionProvider = context.resources.analytical_database

    context.log.info(f"Выгрузка данных...")

    query = f"""
            with 
                filter_max_upd as 
                    (select identity_id , max(updated_at) as max_upd
                    from membership m
                    where updated_at < '{end.date()}'
                    group by identity_id),
                identity_agg as
                    (select distinct id, created_at
                    from identity i)
            select m.id id, m.identity_id identity_id, m.tariff_id tariff_id, m.next_tariff_id next_tariff_id, 
                m.expires_at::Timestamp expires_at, 
                m.tariff_changed_at tariff_changed_at, m.cancelled cancelled, m.updated_at::Timestamp updated_at, 
                m.inserted_at::Timestamp inserted_at, ia.created_at::Timestamp as i_created_at, 
                t.period_name period_name
            from membership m 
                join filter_max_upd fmu on 
                    m.identity_id = fmu.identity_id
                    and m.updated_at = fmu.max_upd
                join identity_agg ia on
                    m.identity_id = ia.id
                left join tariff t on
                    m.tariff_id = t.id
                """

    context.log.debug(f"Выполнение запроса:\n{query}")

    df = provider.execute_query_to_dataframe(query)

    context.log.info(f"Запрос выполнен! {len(df)} строк выгружено!")

    if len(df) > 0:
        yield Output(
            df,
            metadata={"Количество профилей": len(df)}
        )
    else:
        yield Output(
            pd.DataFrame(),
            metadata={"Количество профилей": 0}
        )


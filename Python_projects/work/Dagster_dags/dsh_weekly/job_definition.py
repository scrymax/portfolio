from datetime import datetime

from dagster import define_asset_job, WeeklyPartitionsDefinition, \
    build_schedule_from_partitioned_job

weekly_partitions_def = WeeklyPartitionsDefinition(start_date=datetime(2023, 7, 31),
                                                   timezone="Europe/Moscow",
                                                   day_offset=1,
                                                   minute_offset=15)

dsh_if_plus_weekly = define_asset_job(
    name="dsh_if_plus_weekly",
    selection=["prev_monday_all", "this_monday_all", "tariff_orders_week", "dsh_if_ym_new_user",\
               "prev_monday_act", "this_monday_act", "subscribers_transform", "dsh_if_ch_insert"],
    description="""
    Еженедельный отчёт по подписчикам
    для отдела маркетинга
    """,
    partitions_def=weekly_partitions_def,
)

dsh_if_plus_weekly_sch = build_schedule_from_partitioned_job(
    dsh_if_plus_weekly,
)

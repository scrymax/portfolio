import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import date, timedelta

from ---.jobs.dsh_if_plus_weekly.job_definition import weekly_partitions_def
from ---.resources.databases.clickhouse import ClickhouseConnectionProvider


@asset(
    required_resource_keys={"analytical_database"},
    partitions_def=weekly_partitions_def,
    output_required=False,
    description='Активные подписчики на этот понедельник'
)
def this_monday_act(context: OpExecutionContext, this_monday_all) -> pd.DataFrame:
    """
    Получение информации об активных пользователях на текущий понедельник
    """

    start, end = context.partition_time_window

    end_dt = end.date() - timedelta(days=1)

    df_this_monday_act = this_monday_all.query('expires_at.dt.date > @end_dt and ~tariff_id.isnull()')

    if len(df_this_monday_act) > 0:
        yield Output(
            df_this_monday_act,
            metadata={"Количество профилей": len(df_this_monday_act)}
        )
    else:
        yield Output(
            pd.DataFrame(),
            metadata={"Количество профилей": 0}
        )
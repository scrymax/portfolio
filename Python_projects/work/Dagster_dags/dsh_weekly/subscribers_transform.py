import pandas as pd
from datetime import timedelta
from dagster import asset, OpExecutionContext, Output

from ---.jobs.dsh_if_plus_weekly.job_definition import weekly_partitions_def
from ---.resources.databases.clickhouse import ClickhouseConnectionProvider


@asset(required_resource_keys={"analytical_database"},
       partitions_def=weekly_partitions_def,
       description='Трансформация'
       )
def subscribers_transform(context: OpExecutionContext,
                          prev_monday_all: pd.DataFrame,
                          this_monday_all: pd.DataFrame,
                          prev_monday_act: pd.DataFrame,
                          this_monday_act: pd.DataFrame,
                          tariff_orders_week: pd.DataFrame,
                          dsh_if_ym_new_user: pd.DataFrame):

    """
    Сбор данных из всех джоб и приведение их к виду словаря
    """

    start, end = context.partition_time_window
    context.log.info(f"start - {start.date()}, end - {end.date()}")

    #Регистрации
    context.log.info("Подсчёт регистраций...")
    context.log.debug(f"{this_monday_all.shape}")
    min_date = this_monday_all.query('i_created_at.dt.date >= @start.date() and i_created_at.dt.date <@end.date()')\
        ['i_created_at'].min()
    context.log.debug(f"{min_date}")
    regs = this_monday_all.query('i_created_at.dt.date >= @start.date() and i_created_at.dt.date <@end.date()').shape[0]
    context.log.info(f"Регистраций - {regs}")

    #Новые посетители сайта
    context.log.info("Подсчёт посетителей...")
    new_viewers = dsh_if_ym_new_user.iloc[0][0]
    context.log.info(f"Новых посетителей сайта - {new_viewers}")

    # Отток
    context.log.info("Подсчёт оттока...")
    df_churn = prev_monday_act.merge(this_monday_act, how='left', left_on='prev_identity_id', right_on='identity_id')
    churn = df_churn.loc[df_churn['expires_at'].isnull()].shape[0]
    context.log.info(f"Отток - {churn}")

    #Прочие расчёты
    df_tm = this_monday_act[['identity_id', 'expires_at', 'updated_at']]
    df_tm['col'] = 1

    df_pm = prev_monday_act[['prev_identity_id', 'prev_expires_at']]
    df_pm['col'] = 1

    df_tm_pm = df_tm.merge(df_pm, how='outer', left_on='identity_id', right_on='prev_identity_id')

    df_tm_pm2 = df_tm_pm.loc[df_tm_pm['col_x'] != df_tm_pm['col_y']]
    df_tm_pm2 = df_tm_pm2.loc[df_tm_pm2['col_x'] == 1][['identity_id', 'expires_at', 'updated_at', 'col_x']]

    context.log.debug(f"{prev_monday_all.columns}")
    df_pma = prev_monday_all[['prev_identity_id', 'prev_expires_at', 'prev_i_created_at']]
    df_pma['col'] = 1

    df_tm_pm3 = df_tm_pm2.merge(df_pma, how='left', left_on='identity_id', right_on='prev_identity_id')

    df_newz1 = df_tm_pm3.loc[df_tm_pm3['prev_expires_at'].dt.date == df_tm_pm3['prev_i_created_at'].dt.date][
        'identity_id']
    df_newz2 = df_tm_pm3.loc[df_tm_pm3['prev_identity_id'].isnull()]['identity_id']
    df_newz_all = pd.concat([df_newz1, df_newz2], ignore_index=True)
    df_newz_all = pd.DataFrame(df_newz_all)
    df_newz_all['identity_id'] = df_newz_all['identity_id'].astype(str)
    df_newz_paid = df_newz_all.merge(tariff_orders_week, how='left', left_on='identity_id', right_on='customer_id')
    news_all = df_newz_paid['identity_id'].nunique()
    df_newz_paid = df_newz_paid.loc[~df_newz_paid['updated_at'].isnull()]
    new_payz = df_newz_paid['identity_id'].nunique()

    context.log.info(f"Новых платящих пользователей - {new_payz}")

    def period_name(x):
        if x.lower().find('год') > 0:
            return 'Год'
        else:
            return 'Месяц'

    df_newz_paid['period'] = df_newz_paid['name'].apply(period_name)
    news_year = df_newz_paid.query("period == 'Год'")['identity_id'].count()
    context.log.info(f"Новык годовых пользователей - {news_year}")

    others = news_all - new_payz
    context.log.info(f"Другие - {others}")

    df_tm_pm4 = pd.DataFrame(
        df_tm_pm3.loc[df_tm_pm3['prev_expires_at'].dt.date != df_tm_pm3['prev_i_created_at'].dt.date]. \
        loc[~df_tm_pm3['prev_identity_id'].isnull()][['identity_id', 'updated_at', 'prev_expires_at']])

    df_tm_pm4['diff'] = df_tm_pm4['updated_at'] - df_tm_pm4['prev_expires_at']

    def diff_dates(delta):
        if delta < timedelta(days=30):
            return 'до 30 дней'
        elif delta >= timedelta(days=30):
            return 'более 30 дней'
        else:
            return 'Прочее'

    df_tm_pm4['status'] = df_tm_pm4['diff'].apply(diff_dates)
    before_30_days = df_tm_pm4.query("status == 'до 30 дней'")['identity_id'].count()
    context.log.info(f"Вернувшиеся(до 30 дней) - {before_30_days}")

    after_30_days = df_tm_pm4.query("status == 'более 30 дней'")['identity_id'].count()
    context.log.info(f"Вернувшиеся(более 30 дней) - {after_30_days}")

    main_dict = {
                'memberships': this_monday_act.shape[0],
                'news': new_payz,
                'churn': churn,
                'return_before_30': before_30_days,
                'return_after_30': after_30_days,
                'other': others,
                'new_visits': new_viewers,
                'registrations': regs,
                'yearly_news': news_year
                }

    context.log.info(f"{main_dict}")

    yield Output(main_dict)



import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import date, timedelta

from ---.jobs.dsh_if_plus_weekly.job_definition import weekly_partitions_def
from ---.resources.databases.clickhouse import ClickhouseConnectionProvider


@asset(
    required_resource_keys={"analytical_database"},
    partitions_def=weekly_partitions_def,
    output_required=False,
    description='Активные подписчики на прошлый понедельник'
)
def prev_monday_act(context: OpExecutionContext, prev_monday_all) -> pd.DataFrame:
    """
    Получение информации об активных пользователях на прошлый понедельник
    """
    start, end = context.partition_time_window

    df_prev_monday_act = prev_monday_all.query('prev_expires_at.dt.date >= @start.date() and ~prev_tariff_id.isnull()')

    if len(df_prev_monday_act) > 0:
        yield Output(
            df_prev_monday_act,
            metadata={"Количество профилей": len(df_prev_monday_act)}
        )
    else:
        yield Output(
            pd.DataFrame(),
            metadata={"Количество профилей": 0}
        )

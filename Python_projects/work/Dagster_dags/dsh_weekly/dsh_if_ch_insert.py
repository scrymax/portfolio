import pandas as pd
from datetime import timedelta
from dagster import asset, OpExecutionContext

from ---.jobs.dsh_if_plus_weekly.job_definition import weekly_partitions_def
from ---.resources.databases.clickhouse import ClickhouseConnectionProvider


@asset(required_resource_keys={"analytical_database"},
       partitions_def=weekly_partitions_def,
       description='Загрузка в Clickhouse'
       )
def dsh_if_ch_insert(context: OpExecutionContext, subscribers_transform):
    """
    Загрузка данных в две таблицы в кликхаус
    """

    start, end = context.partition_time_window
    
    context.log.info("Создание первого датафрейма...")
    df_ins1 = pd.DataFrame.from_dict(subscribers_transform, orient='index').\
        reset_index().rename(columns={'index': 'types', 0: 'values'})
    df_ins1['date'] = end.date()

    context.log.info("Создание второго датафрейма...")
    df_ins2 = df_ins1.transpose()
    df_ins2 = df_ins2.reset_index()
    df_ins2.columns = df_ins2.iloc[0]
    df_ins2 = df_ins2[1:2]
    df_ins2 = df_ins2.drop(columns=['types'])
    df_ins2['date'] = end.date()

    context.log.info(f"Загрузка  профилей...")
    provider_ch: ClickhouseConnectionProvider = context.resources.analytical_database

    context.log.info(f"Загрузка данных в  weekly_analytics_main...")
    provider_ch.insert_dataframe('INSERT INTO weekly_analytics_main VALUES', df_ins1)

    context.log.info(f"Загрузка данных в  weekly_analytics...")
    provider_ch.insert_dataframe('INSERT INTO weekly_analytics VALUES', df_ins2)



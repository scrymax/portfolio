import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import datetime
import os

from ---.jobs.report_bot.job_definition import daily_partitions_def
from ---.resources.databases.postgres import PostgresConnectionProviderPsycopg


@asset(
    required_resource_keys={"if_plus_main_database"},
    partitions_def=daily_partitions_def,
    description="Активные тарифы",
    output_required=False
)
def users_30_day_off(context: OpExecutionContext) -> pd.DataFrame:
    """
    Получение данных пользователях без подписки более 30 дней
    """
    context.log.info("Подключаемся к Postgre...")
    provider: PostgresConnectionProviderPsycopg = context.resources.if_plus_main_database

    query = f'''
        with 
            membership as 		
                (SELECT identity_id, 
                    expires_at, 
                    tariff_id as tariff_period_id, 
                    next_tariff_id as new_tariff_period_id,
                    cancelled, 
                    updated_at
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select identity_id, 
                                expires_at, 
                                tariff_id,
                                next_tariff_id, 
                                cancelled,
                                updated_at from ---') AS t1
                                (identity_id UUID, 
                                    expires_at timestamptz,
                                    tariff_id UUID,  
                                    next_tariff_id UUID, 
                                    cancelled Bool, 
                                    updated_at timestamptz)),
            user_identity as 		
                (SELECT id, phone_number, email, created_at, updated_at, deleted_at
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select id, phone_number, email, created_at, updated_at, deleted_at from ---') AS t1
                            (id UUID, phone_number Varchar, email Varchar , created_at timestamptz, updated_at timestamptz,deleted_at timestamptz))
        select ui.email , date(m.expires_at) as "Окончание подписки"
        from membership m 
            join user_identity ui on
                m.identity_id = ui.id 
        where m.expires_at < current_date - 30 
            and ui.email is not null
            and ui.deleted_at is null
            and date(m.expires_at) <> date(ui.created_at)
        order by m.expires_at desc'''

    off_users = provider.execute_query_to_dataframe(query).set_index('email')

    off_users['Окончание подписки'] = off_users['Окончание подписки']. \
        apply(lambda x: datetime.strftime(x, "%d.%m.%Y"))

    yield Output(off_users)

import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import datetime
import os

from ---.jobs.report_bot.job_definition import daily_partitions_def
from ---.resources.databases.postgres import PostgresConnectionProviderPsycopg


@asset(
    required_resource_keys={"if_plus_main_database"},
    partitions_def=daily_partitions_def,
    description="Архивные тарифы",
    output_required=False
)
def archive_tariff(context: OpExecutionContext) -> pd.DataFrame:
    """
    Получение информации о пользователях с типо подписки Архивный
    """
    context.log.info("Подключаемся к Postgre...")
    provider: PostgresConnectionProviderPsycopg = context.resources.if_plus_main_database

    query = f'''
         with 
            membership as 		
                (SELECT identity_id, 
                    expires_at, 
                    tariff_id as tariff_period_id, 
                    next_tariff_id as new_tariff_period_id,
                    cancelled, 
                    updated_at
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select identity_id, 
                                expires_at, 
                                tariff_id,
                                next_tariff_id, 
                                cancelled,
                                updated_at from ---') AS t1
                                (identity_id UUID, 
                                    expires_at timestamptz,
                                    tariff_id UUID,  
                                    next_tariff_id UUID, 
                                    cancelled Bool, 
                                    updated_at timestamptz)),
            membership_tariff_period as 
                (SELECT id, is_active as active, tariff_family_id, period_name
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select id, is_active, tariff_family_id, period_name from ---') AS t1
                            (id UUID, is_active bool, tariff_family_id UUID, period_name varchar(255))),
            membership_tariff as 
                (SELECT id, family_name as cart_name
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select id, family_name from ---') AS t1
                            (id UUID, family_name varchar(255))),
            user_identity as 		
                (SELECT id, phone_number, email, created_at, updated_at, deleted_at
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select id, phone_number, email, created_at, updated_at, deleted_at from ---') AS t1
                            (id UUID, phone_number Varchar, email Varchar , created_at timestamptz, updated_at timestamptz,deleted_at timestamptz))
        select ui.email , 
            mt.cart_name as "Тариф" , 
            mtp.period_name as "Период" , 
            pp2.price as "Расшифровка",
            date(m.expires_at) as "Дата окончания подписки"
        from membership_tariff_period mtp 
            join membership_tariff mt on
                mtp.tariff_family_id = mt.id 
            join membership m on
                mtp.id = m.tariff_period_id
            join user_identity ui on
                m.identity_id = ui.id 
            join tariff_product tp on
                tp.tariff_id = m.tariff_period_id
            join product p on
                tp.product_ptr_id = p.id
            join product_prices pp on
                p.id = pp.product_id 
            join product_price pp2 on
                pp.productprice_id = pp2.id 
                and pp2.currency = 'RUB'
        where m.expires_at > now() 
            and mtp.active=False 
            and ui.email is not null'''

    arch_users = provider.execute_query_to_dataframe(query).set_index('email')

    arch_users['Дата окончания подписки'] = arch_users['Дата окончания подписки']. \
        apply(lambda x: datetime.strftime(x, "%d.%m.%Y"))

    yield Output(arch_users)

import pandas as pd
from dagster import asset, OpExecutionContext, Output
from datetime import datetime
import os

from ---.jobs.report_bot.job_definition import daily_partitions_def
from ---.resources.databases.postgres import PostgresConnectionProviderPsycopg


@asset(
    required_resource_keys={"if_plus_main_database"},
    partitions_def=daily_partitions_def,
    description="Активные тарифы",
    output_required=False
)
def active_tariff(context: OpExecutionContext) -> pd.DataFrame:
    """
    Получение информации о пользователях с типо подписки Активный
    """
    context.log.info("Подключаемся к Postgre...")
    provider: PostgresConnectionProviderPsycopg = context.resources.if_plus_main_database

    query = f'''
        with 
            membership as 		
                (SELECT identity_id, 
                    expires_at, 
                    tariff_id as tariff_period_id, 
                    next_tariff_id as new_tariff_period_id,
                    cancelled, 
                    updated_at
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select identity_id, 
                                expires_at, 
                                tariff_id,
                                next_tariff_id, 
                                cancelled,
                                updated_at from ---') AS t1
                                (identity_id UUID, 
                                    expires_at timestamptz,
                                    tariff_id UUID,  
                                    next_tariff_id UUID, 
                                    cancelled Bool, 
                                    updated_at timestamptz)),
            membership_tariff_period as 
                (SELECT id, is_active as active, tariff_family_id, period_name
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select id, is_active, tariff_family_id, period_name from ---') AS t1
                            (id UUID, is_active bool, tariff_family_id UUID, period_name varchar(255))),
            membership_tariff as 
                (SELECT id, family_name as cart_name
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select id, family_name from ---') AS t1
                            (id UUID, family_name varchar(255))),
            user_identity as 		
                (SELECT id, phone_number, email, created_at, updated_at, deleted_at
                FROM dblink('dbname=--- 
                            host = {os.getenv("---")} 
                            port = {os.getenv("---")} 
                            user = {os.getenv("---")} 
                            password = {os.getenv("---")}',
                            'select id, phone_number, email, created_at, updated_at, deleted_at from ---') AS t1
                            (id UUID, phone_number Varchar, email Varchar , created_at timestamptz, updated_at timestamptz,deleted_at timestamptz))
        select ui.email , 
            mt.cart_name as "Тариф" , 
            mtp.period_name as "Период" ,  
            date(m.expires_at) as "Дата окончания подписки"
        from membership_tariff_period mtp 
            join membership_tariff mt on
                mtp.tariff_family_id = mt.id 
            join membership m on
                mtp.id = m.tariff_period_id
            join user_identity ui on
                m.identity_id = ui.id 
        where m.expires_at > now() 
            and mtp.active 
            and ui.email is not null'''

    act_users = provider.execute_query_to_dataframe(query).set_index('email')

    act_users['Дата окончания подписки'] = act_users['Дата окончания подписки']. \
        apply(lambda x: datetime.strftime(x, "%d.%m.%Y"))

    yield Output(act_users)

import pandas as pd
import telegram
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import datetime, date
import io
import asyncio
import os

from ---.jobs.report_bot.job_definition import daily_partitions_def


@asset(
    partitions_def=daily_partitions_def,
    description="Отправка в тг",
    output_required=False
)
def tg_report(context: OpExecutionContext, archive_tariff, active_tariff, users_30_day_off):
    """
    Сбор отчетов в один файл .xlsx и отправка в телеграм в чат с отчетами
    """

    context.log.info("Объединение в один файл...")
    tok = os.getenv("---")
    context.log.debug(f"token = {tok}")
    chat_id = os.getenv("---")
    context.log.debug(f"token = {chat_id}")
    bot = telegram.Bot(token=tok)

    output = io.BytesIO()

    with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
        archive_tariff.to_excel(writer, sheet_name='Архивный тариф')
        active_tariff.to_excel(writer, sheet_name='Активный тариф')
        users_30_day_off.to_excel(writer, sheet_name='Без подписки более 30 дней')

    # Передача указателя на начало файла
    output.seek(0)

    day = datetime.strftime(date.today(), "%d.%m.%Y")

    # Отправка файла
    async def send_file():
        await bot.send_document(chat_id=chat_id, document=output, filename=f'Выгрузка пользователей за {day}.xlsx')

    # Проверка активности цикла asyncio
    if asyncio.get_event_loop().is_running():
        asyncio.create_task(send_file())
    else:
        asyncio.run(send_file())


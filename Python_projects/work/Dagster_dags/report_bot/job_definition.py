from datetime import datetime

from dagster import define_asset_job, DailyPartitionsDefinition, \
    build_schedule_from_partitioned_job

daily_partitions_def = DailyPartitionsDefinition(start_date=datetime(2022, 12, 7),
                                                 timezone="Europe/Moscow",
                                                 hour_offset=9)

report_bot = define_asset_job(
    name="report_bot",
    selection=["archive_tariff", "active_tariff" , "users_30_day_off", "tg_report"],
    description="""
    Эта джоба ежедневно отправляет файл с актуальной информацией 
    по подписчикам отделу маркетинга в телеграм
    """,
    partitions_def=daily_partitions_def,
)

report_bot_ch = build_schedule_from_partitioned_job(
    report_bot,
)
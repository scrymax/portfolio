import pandas as pd
import os
import requests
import time
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import date, timedelta

from ---.jobs.getcourse_events.job_definition import hourly_partitions_def



@asset(
    partitions_def=hourly_partitions_def,
    output_required=False,
    description='Экспорт из геткурса платежей'
)
def gc_events_payments(context: OpExecutionContext, gc_events_users) -> pd.DataFrame:
    """
    Подключение по апи к кабинету Getcourse и получение информации о платежах
    """

    start, end = context.partition_time_window

    start = start.replace(tzinfo=None)
    end = end.replace(tzinfo=None)


    context.log.info(f"Запрос у GetCourse id отчёта оплат, дата - {start.date()-timedelta(days=1)} : {start.date()}")

    key = os.getenv("---"),
    URL = 'https://---.online/pl/api/account/payments'
    params = {
        'key': key,
        'status_changed_at[from]': start.date()-timedelta(days=1),
        'status_changed_at[to]': end.date()
    }

    for x in range(0, 10):
        gc = requests.get(URL, params=params)
        if gc.json()['success']:
            break
        else:
            print(f'Попытка получения id отчета оплат - {x + 1}')
            time.sleep(30)

    export_id = gc.json()['info']['export_id']
    gc.connection.close()

    context.log.info(f"Получение отчета оплат {export_id}")

    time.sleep(30)

    URL = f'https://---.online/pl/api/account/exports/{export_id}'
    params = {
        'key': key
    }

    df = pd.DataFrame()

    for x in range(0, 10):
        gc_data = requests.get(URL, params=params)
        if gc_data.json()['success']:
            df = pd.DataFrame(gc_data.json()['info']['items'], columns=gc_data.json()['info']['fields'])
            break
        elif gc_data.json()['error_code'] == 910:
            break
        else:
            time.sleep(10)

    gc_data.connection.close()

    context.log.info(f"Успешно подключились! Извлечено {len(df)} строк отчёта оплат")

    yield Output(
        df,
        metadata={"Платежей в этот день": len(df)}
    )

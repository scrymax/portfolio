import pandas as pd
from dagster import asset, OpExecutionContext

from ---.jobs.getcourse_events.job_definition import hourly_partitions_def
from ---.resources.databases.clickhouse import ClickhouseConnectionProvider


@asset(required_resource_keys={"analytical_database"},
       partitions_def=hourly_partitions_def,
       description='Загрузка в Clickhouse'
       )
def gc_events_insert_ch(context: OpExecutionContext,
                         gc_events_regs_transform,
                         gc_events_users_transform,
                         gc_events_payments_transform):

    """
    Загрузка данных в кликхаус в таблицы с движком ReplacingMergeTree, оптимизация таблиц после вставки
    """   

    provider_ch: ClickhouseConnectionProvider = context.resources.analytical_database

    context.log.info(f"Загрузка в Clickhouse...")

    if len(gc_events_regs_transform)>0:
        provider_ch.insert_dataframe('INSERT INTO gc_events_regs VALUES', gc_events_regs_transform)
        provider_ch.optimize_table('gc_events_regs')

    if len(gc_events_users_transform)>0:
        provider_ch.insert_dataframe('INSERT INTO gc_events_users VALUES', gc_events_users_transform)
        provider_ch.optimize_table('gc_events_users')

    if len(gc_events_payments_transform)>0:
        provider_ch.insert_dataframe('INSERT INTO gc_events_payments VALUES', gc_events_payments_transform)
        provider_ch.optimize_table('gc_events_payments')



    context.log.info(f"Заказы - {len(gc_events_regs_transform)} строк")
    context.log.info(f"Пользователи - {len(gc_events_users_transform)} строк")
    context.log.info(f"Платежи - {len(gc_events_payments_transform)} строк")
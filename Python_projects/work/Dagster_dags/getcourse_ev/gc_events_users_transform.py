import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output

from ---.jobs.getcourse_events.job_definition import hourly_partitions_def


@asset(
    partitions_def=hourly_partitions_def,
    output_required=False,
    description='Обработка отчёта пользователей'
)
def gc_events_users_transform(context: OpExecutionContext, gc_events_users: pd.DataFrame) -> pd.DataFrame:
    start, end = context.partition_time_window

    start = start.replace(tzinfo=None)
    end = end.replace(tzinfo=None)

    context.log.info(f"Обработка отчёта")
    try:
        df_users = gc_events_users.copy()

        df_users = df_users.rename(columns={'Email':'email',
                                          'Тип регистрации':'reg_type',
                                          'Создан':'created_at',
                                          'Имя':'firstname',
                                          'Фамилия':'surname',
                                          'Телефон':'phone_number',
                                          'Дата рождения':'birthday',
                                          'Возраст':'age',
                                          'Страна':'country',
                                          'Город':'city',
                                          'От партнера':'from_partner',
                                          'Откуда пришел':'come_from',
                                          'ID партнера':'partner_id',
                                          'Email партнера':'partner_email',
                                          'ФИО партнера':'partner_name',
                                          'ФИО менеджера':'manager_name',
                                          'VK-ID':'vk_id'})

        df_users = df_users.loc[:, df_users.columns != 'Последняя активность']

        df_users = df_users.replace('', None)

        df_users['created_at'] = pd.to_datetime(df_users['created_at'])
        df_users['birthday'] = pd.to_datetime(df_users['birthday'], errors='coerce').dt.date

        def phone_cleaner(x):
            if pd.notnull(x) and x[0] != '+':
                return '+' + x
            else:
                return x

        df_users['phone_number'] = df_users['phone_number'].apply(phone_cleaner)
        df_users['age'] = df_users['age'].apply(lambda x: int(x) if pd.notnull(x) else x)

        #df_users = df_users.query('created_at > @start and created_at < @end')

        context.log.info(f"Обработано {len(df_users)} строк отчёта")

    except:
        df_users = pd.DataFrame()

    yield Output(
        df_users,
        metadata={"Пользователей": len(df_users)}
    )
        
import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output

from ---.jobs.getcourse_events.job_definition import hourly_partitions_def


@asset(
    partitions_def=hourly_partitions_def,
    output_required=False,
    description='Обработка отчёта заказов'
)
def gc_events_regs_transform(context: OpExecutionContext, gc_events_regs: pd.DataFrame) -> pd.DataFrame:
    start, end = context.partition_time_window

    start = start.replace(tzinfo=None)
    end = end.replace(tzinfo=None)

    context.log.info(f"Обработка отчёта")
    try:
        df_regs = gc_events_regs.copy()

        df_regs = df_regs.rename(columns={'ID заказа': 'reg_id',
                                          'Номер': 'order_id',
                                          'ID пользователя': 'user_id',
                                          'Дата создания': 'created_at',
                                          'Title': 'product_name',
                                          'Стоимость, RUB': 'price',
                                          'Валюта': 'currency',
                                          'Менеджер': 'manager',
                                          'Город': 'city',
                                          'Платежная система': 'payment_system',
                                          'ID партнера': 'partner_id',
                                          'Использован промокод': 'promocode',
                                          'Промоакция': 'promo_action',
                                          'ID партнера заказа': 'order_partner_id',
                                          'Email партнера заказа': 'order_partner_email',
                                          'Теги': 'tag',
                                          'Теги предложений': 'tag_offer'})

        columns_list = list(df_regs.columns)

        utm_source_list = []
        utm_medium_list = []
        utm_campaign_list = []
        utm_content_list = []
        utm_term_list = []

        #Поскольку встречаются повторяющиеся заголовки, забираем необходимые, идущие вторыми
        for column, value in enumerate(columns_list):
            if value == 'utm_source':
                utm_source_list.append(column)
            elif value == 'utm_medium':
                utm_medium_list.append(column)
            elif value == 'utm_campaign':
                utm_campaign_list.append(column)
            elif value == 'utm_content':
                utm_content_list.append(column)
            elif value == 'utm_term':
                utm_term_list.append(column)

        columns_list[utm_source_list[1]] = 'utm_source_gc'
        columns_list[utm_medium_list[1]] = 'utm_medium_gc'
        columns_list[utm_campaign_list[1]] = 'utm_campaign_gc'
        columns_list[utm_content_list[1]] = 'utm_content_gc'
        columns_list[utm_term_list[1]] = 'utm_term_gc'

        df_regs.columns = columns_list

        df_regs = df_regs[['reg_id', 'order_id', 'user_id',
                           'created_at', 'product_name', 'price', 'currency', 'manager', 'city',
                           'payment_system', 'partner_id', 'promocode', 'promo_action',
                           'utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term',
                           'order_partner_id', 'order_partner_email',
                           'utm_source_gc', 'utm_medium_gc', 'utm_campaign_gc', 'utm_content_gc',
                           'utm_term_gc', 'utm_group', 'user_utm_source', 'user_utm_medium',
                           'user_utm_campaign', 'user_utm_content', 'user_utm_term',
                           'user_utm_group', 'user_gcpc', 'tag', 'tag_offer']]

        df_regs = df_regs.replace('', None)

        df_regs['order_id'] = df_regs['order_id'].apply(lambda x: int(x))
        df_regs['created_at'] = pd.to_datetime(df_regs['created_at'])
        df_regs['price'] = df_regs['price'].apply(lambda x: float(x))
        df_regs['tag'] = df_regs['tag'].apply(lambda x: str(x))
        df_regs['tag_offer'] = df_regs['tag_offer'].apply(lambda x: str(x))

        context.log.info(f"Обработано {len(df_regs)} строк отчёта")

    except:
        df_regs = pd.DataFrame()

    yield Output(
        df_regs,
        metadata={"Profiles count": len(df_regs)}
    )
        
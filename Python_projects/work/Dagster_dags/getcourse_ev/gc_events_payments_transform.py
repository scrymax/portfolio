import pandas as pd
from dagster import asset, OpExecutionContext, AssetOut, Output

from ---.jobs.getcourse_events.job_definition import hourly_partitions_def


@asset(
    partitions_def=hourly_partitions_def,
    output_required=False,
    description='Обработка отчёта оплат'
)
def gc_events_payments_transform(context: OpExecutionContext, gc_events_payments: pd.DataFrame) -> pd.DataFrame:
    start, end = context.partition_time_window

    start = start.replace(tzinfo=None)
    end = end.replace(tzinfo=None)

    context.log.info(f"Обработка отчёта")
    try:
        df_plat = gc_events_payments.copy()

        df_plat = df_plat.rename(columns={'Номер': 'payment_id',
                                          'Заказ': 'order_id',
                                          'Дата создания': 'created_at',
                                          'Тип': 'payment_type',
                                          'Статус': 'payment_status',
                                          'Сумма': 'payment_price',
                                          'Комиссии': 'payment_comission',
                                          'Получено': 'payment_received',
                                          'Код платежа': 'payment_code'})
        df_plat = df_plat[['payment_id', 'order_id', 'created_at', 'payment_type', 'payment_status',
                           'payment_price', 'payment_comission', 'payment_received', 'payment_code']]

        df_plat['created_at'] = pd.to_datetime(df_plat['created_at'])
        df_plat['payment_price'] = df_plat['payment_price'].apply(lambda x: x.replace(' ', '').replace('руб.', ''))
        df_plat['payment_comission'] = df_plat['payment_comission'].\
            apply(lambda x: x.replace(' ', '').replace('руб.', ''))
        df_plat['payment_received'] = df_plat['payment_received'].\
            apply(lambda x: x.replace(' ', '').replace('руб.', ''))

        df_plat = df_plat.astype({'payment_price': 'float',
                                  'payment_comission': 'float',
                                  'payment_received': 'float'})

        df_plat = df_plat.replace('', None)

        #df_plat = df_plat.query('created_at > @start and created_at < @end')

        context.log.info(f"Обработано {len(df_plat)} строк отчёта")

    except:
        df_plat = pd.DataFrame()

    yield Output(
        df_plat,
        metadata={"Платежей": len(df_plat)}
    )
        
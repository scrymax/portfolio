from datetime import datetime

from dagster import define_asset_job, HourlyPartitionsDefinition, \
    build_schedule_from_partitioned_job

hourly_partitions_def = HourlyPartitionsDefinition(start_date=datetime(2023, 7, 13, 22, 0, 0),
                                                   timezone="Europe/Moscow",
                                                   minute_offset=5)

getcourse_events = define_asset_job(
    name="getcourse_events",
    selection=["gc_events_regs",
               "gc_events_regs_transform",
               "gc_events_users",
               "gc_events_users_transform",
               "gc_events_payments",
               "gc_events_payments_transform",
               "gc_events_insert_ch"],
    description="""
    Эта джоба собирает данные о 
    регистрациях, оплатах и пользователях 
    из геткурса Events
    """,
    partitions_def=hourly_partitions_def,
)

getcourse_events_schedule = build_schedule_from_partitioned_job(
    getcourse_events,
)
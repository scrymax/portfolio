import pandas as pd
import os
import requests
import json
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import date, timedelta

from ---.jobs.kinescope_efir.job_definition import daily_partitions_def



@asset(
    partitions_def=daily_partitions_def,
    output_required=False,
    description='Экспорт метрик для видео'
)
def kinescope_video_views(context: OpExecutionContext, kinescope_folders):
    """
    Запрос у кинескоп необходимых метрик видео за день, трансформация
    """

    start, end = context.partition_time_window

    start_date = start.date()
    end_date = end.date()

    context.log.info(f"Запрос у Kinescope метрик видео, даты - {start} : {end}")

    url = "https://api.kinescope.io/v1/analytics"
    payload = {}
    headers = {'Authorization': f'Bearer {os.getenv("---")}'}
    params = {
        'from': start_date,
        'to': end_date,
        'group': 'day',
        'group_entities': ['video_id'],
        'fields': ['folder_id','view', 'play','city', 'device','seek',\
                   'country_code','player_load', 'watch_time', 'external_id'],
    }

    response = requests.request("GET", url, params=params, headers=headers, data=payload)

    df_views = pd.DataFrame(response.json()['data'])

    context.log.info(f"Успешно подключились! Извлечено {len(df_views)} строк")

    df_views['date'] = (pd.to_datetime(df_views['date']) + timedelta(hours=3)).dt.date

    df_views = df_views.query('folder_id in @kinescope_folders and date<@end_date')

    def json_live(x):
        try:
            return json.loads(str(x).replace('&quot;', '"'))['isLive']
        except:
            return None

    def json_users(x):
        try:
            return json.loads(x.replace('&quot;', '"'))['userId']
        except:
            return None

    df_views['is_live'] = df_views['external_id'].apply(json_live)
    df_views['user_id'] = df_views['external_id'].apply(json_users)

    df_views = df_views[['video_id', 'date', 'is_live', 'user_id', 'device', 'city', 'country_code',
                         'view', 'play', 'watch_time', 'seek', 'player_load']]

    df_views = df_views.query("~is_live.isnull()")

    context.log.info(f"Результатов по папке IF+ Efir -  {len(df_views)} строк")

    yield Output(
        df_views,
        metadata={"Строк для загрузки": len(df_views)}
    )

from datetime import date, datetime

from dagster import define_asset_job, DailyPartitionsDefinition, \
    build_schedule_from_partitioned_job

daily_partitions_def = DailyPartitionsDefinition(start_date=datetime(2023, 8, 22),
                                                 timezone="Europe/Moscow",
                                                 hour_offset=2)

kinescope_efir = define_asset_job(
    name="kinescope_efir",
    selection=["kinescope_folders",
               "kinescope_video_list",
               "kinescope_video_views",
               "kinescope_insert_ch"
               ],
    description="""
    Эта джоба собирает данные об 
    эфирах из Kinescope
    """,
    partitions_def=daily_partitions_def,
)

kinescope_efir_sch = build_schedule_from_partitioned_job(
    kinescope_efir,
)
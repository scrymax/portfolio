import pandas as pd
import os
import requests
import time
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import date, timedelta

from ---.jobs.kinescope_efir.job_definition import daily_partitions_def



@asset(
    partitions_def=daily_partitions_def,
    output_required=False,
    description='Экспорт списка видео',
)
def kinescope_video_list(context: OpExecutionContext, kinescope_folders):
    """
    Получение у кинескопа списка видео из необходимых папок, трансформация
    """

    start, end = context.partition_time_window

    start = start.replace(tzinfo=None)
    end = end.replace(tzinfo=None)

    start_date = start.date()
    end_date = end.date()

    context.log.info(f"Запрос у GetCourse id списка всех видео ")

    url = "https://api.kinescope.io/v1/videos?"

    payload = {}
    headers = {'Authorization': f'Bearer {os.getenv("---")}'}
    params = {
        'page': 1,
        'per_page': 10000,
        'order': 'created_at.desc,title.asc',
    }

    response = requests.request("GET", url, params=params, headers=headers, data=payload)

    df_videos = pd.DataFrame(response.json()['data'])

    context.log.info(f"Всего видео - {len(df_videos)}")

    df_videos['link'] = df_videos['play_link'].apply(lambda x: x.replace('https://kinescope.io/', ''))
    df_videos = df_videos.query('folder_id in @kinescope_folders')
    context.log.info(f"Всего видео в папке IF+ Efir - {len(df_videos)}")

    df_videos = df_videos[['id', 'title', 'duration', 'created_at', 'updated_at', 'link']]
    df_videos['created_at'] = pd.to_datetime(df_videos['created_at']) + timedelta(hours=3)
    df_videos['updated_at'] = pd.to_datetime(df_videos['updated_at']) + timedelta(hours=3)

    df_videos = df_videos.query('updated_at.dt.date >= @start_date and updated_at.dt.date < @end_date')

    context.log.info(f"Видео для загрузки в базу данных - {len(df_videos)}")

    yield Output(
        df_videos,
        metadata={"Видео для загрузки": len(df_videos)}
    )

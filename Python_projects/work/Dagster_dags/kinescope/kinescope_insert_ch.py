import pandas as pd
from dagster import asset, OpExecutionContext

from ---.jobs.kinescope_efir.job_definition import daily_partitions_def
from ---.resources.databases.clickhouse import ClickhouseConnectionProvider


@asset(required_resource_keys={"analytical_database"},
       partitions_def=daily_partitions_def,
       description='Загрузка в Clickhouse'
       )
def kinescope_insert_ch(context: OpExecutionContext, kinescope_video_list, kinescope_video_views):
    """
    Загрузка данных в кликхаус списка видео и просмотров у них
    """

    provider_ch: ClickhouseConnectionProvider = context.resources.analytical_database

    context.log.info(f"Загрузка в Clickhouse...")

    if len(kinescope_video_list)>0:
        provider_ch.insert_dataframe('INSERT INTO ks_video_list VALUES', kinescope_video_list)
        provider_ch.optimize_table('ks_video_list')

    if len(kinescope_video_views)>0:
        provider_ch.insert_dataframe('INSERT INTO ks_video_views VALUES', kinescope_video_views)
        provider_ch.optimize_table('ks_video_views')

    context.log.info(f"Обновлений видео - {len(kinescope_video_list)}")
    context.log.info(f"Информации о просмотрах - {len(kinescope_video_views)}")
import pandas as pd
import requests
import os
import time
from dagster import asset, OpExecutionContext, AssetOut, Output
from datetime import date, timedelta

from ---.jobs.kinescope_efir.job_definition import daily_partitions_def



@asset(
    partitions_def=daily_partitions_def,
    output_required=False,
    description='Экспорт из кинескопа списка папок'
)
def kinescope_folders(context: OpExecutionContext):

    """
    Запрос у кинескопа всех папок, а также получение списка всех дочерних папок в необходимой
    """

    project = '---'

    context.log.info(f"Запрос у kinescope списка всех папок")

    url = f"https://api.kinescope.io/v1/projects/{project}/folders"

    payload = {}
    headers = {'Authorization': f'Bearer {os.getenv("---")}'}
    params = {
        'page': 1,
        'per_page': 1000}

    response = requests.request("GET", url, params=params, headers=headers, data=payload)

    df_folders = pd.DataFrame(response.json()['data'])

    folder_list = ['---']

    def folders(x):
        for index, row in df_folders.iterrows():
            if row['parent_id'] == x:
                folder_list.append(row['id'])
                folders(row['id'])

    folders(folder_list[0])

    context.log.info(f"Успешно подключились!")

    yield Output(
        folder_list,
        metadata={"Папок для сканирования": len(folder_list)}
    )
